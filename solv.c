/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solv.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edelangh <edelangh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 13:56:02 by edelangh          #+#    #+#             */
/*   Updated: 2015/05/10 14:03:48 by edelangh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

double		ft_sqrt(double d)
{
	double	n;
	double	a;
	int		i;

	i = 50;
	a = (double)d;
	n = 0;
	while (n * n < a)
		n += 0.5;
	while (--i)
	{
		n = (n + (a / n)) / (2);
	}
	return (n);
}

void		ft_solv_input_1d(t_equa *t)
{
	dprintf(1, "Polynomial degree: 1\n");
	dprintf(1, "The solution is:\n%f\n", -(t->c / t->b));
}

void		ft_solv_input_2d(t_equa *t)
{
	dprintf(1, "Polynomial degree: 2\n");
	t->delta = t->b * t->b - 4 * t->a * t->c;
	dprintf(1, "TEST:%f\n", t->a);
	if (t->delta > 0)
	{
		dprintf(1, "Delta > 0\n");
		t->x1 = (-t->b - ft_sqrt(t->delta)) / (2 * t->a);
		t->x2 = (-t->b + ft_sqrt(t->delta)) / (2 * t->a);
		dprintf(1, "The two solutions are reel:\n");
		dprintf(1, "%+f\n%+f\n", t->x1, t->x2);
	}
	else if (t->delta == 0)
	{
		dprintf(1, "Delta = 0\n");
		t->x1 = (-t->b) / (2 * t->a);
		dprintf(1, "The solution is:\n%+f\n", t->x1);
	}
	else
	{
		dprintf(1, "Delta < 0\nThe two solutions are complexe:\n");
		dprintf(1, "z1:%f - %fi\n", -t->b /
				(2 * t->a), ft_sqrt(-t->delta) / (2 * t->a));
		dprintf(1, "z2:%f + %fi\n", -t->b /
				(2 * t->a), ft_sqrt(-t->delta) / (2 * t->a));
	}
}
