/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cpu.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edelangh <edelangh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 10:58:18 by edelangh          #+#    #+#             */
/*   Updated: 2015/05/10 14:03:13 by edelangh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CPU_H
# define CPU_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <ctype.h>

typedef struct	s_equa
{
	char		*equa;
	double		a;
	double		b;
	double		c;
	int			side;
	double		delta;
	double		x1;
	double		x2;
}				t_equa;

void			ft_read_input(t_equa *t, char *s, int i);

double			ft_atof(char *s, int i);
void			ft_puts_input(t_equa *t);
void			ft_nett_input(t_equa *t);
void			ft_strcpy(char *a, char *b);
int				ft_next_digit(char *s, int i);

double			ft_sqrt(double d);
void			ft_solv_input_1d(t_equa *t);
void			ft_solv_input_2d(t_equa *t);

#endif
