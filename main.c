/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edelangh <edelangh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 10:54:03 by edelangh          #+#    #+#             */
/*   Updated: 2015/05/10 14:03:39 by edelangh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

int		ft_get_pow(t_equa *t, int i, double r)
{
	char	c;

	c = t->equa[i] - '0';
	r *= t->side;
	if (c == 0)
		t->c += r;
	else if (c == 1)
		t->b += r;
	else if (c == 2)
		t->a += r;
	++i;
	return (i);
}

int		ft_get_val(t_equa *t, int i)
{
	char	*s;
	double	r;

	s = t->equa;
	r = ft_atof(s, i);
	i = ft_next_digit(s, i);
	if (s[i] == '*' && ++i)
	{
		if (s[i] == 'X' && ++i)
		{
			if (s[i] == '^' && ++i)
				if (s[i] == '0' || s[i] == '1' || s[i] == '2')
					return (ft_get_pow(t, i, r));
				else
					exit(dprintf(2, "Error: Cannot solv X^%c...\n", s[i]));
			else
				t->b += r * t->side;
		}
		else
			exit(dprintf(2, "Error: I can't multiply 2 real numbers\n"));
	}
	else
		t->c += r * t->side;
	return (i);
}

void	ft_read_input(t_equa *t, char *s, int i)
{
	while (t->side = 1, s[++i])
	{
		if (isdigit(s[i]) || s[i] == '-')
			i = ft_get_val(t, i);
		if (!s[i] || s[i] == '=')
			break ;
		if (s[i] == '-')
			--i;
		else if (s[i] != '+')
			exit(dprintf(2, "Error: I can't solv %c at this position\n", s[i]));
	}
	while (t->side = -1, s[++i])
	{
		if (isdigit(s[i]) || s[i] == '-')
			i = ft_get_val(t, i);
		if (!s[i] || s[i] == '=')
			break ;
		if (s[i] == '-')
			--i;
		else if (s[i] != '+')
			exit(dprintf(2, "Error: I can't solv %c at this position\n", s[i]));
	}
}

int		main(int ac, char **av)
{
	t_equa	t;

	if (ac != 2)
		return (dprintf(2, "Usage:%s <equa>\n", av[0]));
	bzero(&t, sizeof(t));
	(void)ac;
	t.equa = strdup(av[1]);
	ft_nett_input(&t);
	ft_read_input(&t, t.equa, -1);
	ft_puts_input(&t);
	if (t.a)
		ft_solv_input_2d(&t);
	else if (t.b)
		ft_solv_input_1d(&t);
	else if (t.c)
		exit(dprintf(2, "Error: No solutions\n"));
	else
		exit(dprintf(2, "All Real are solutions\n"));
	return (0);
}
