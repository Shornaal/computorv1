#var
CC = gcc
NAME = computor
CFLAGS = -Wall -Wextra -Werror
INC =
OBJDIR = obj/
SRCS = main.c \
	   tools.c \
	   solv.c

HEADER = cpu.h
OBJS = $(addprefix $(OBJDIR),$(subst .c,.o,$(SRCS)))
INCLUDES = -I.

.PHONY: all clean fclean re proper

all: $(NAME)

$(NAME): $(OBJS) $(HEADER)
	$(CC) $(CFLAGS) $(INC) $(OBJS) -o $(NAME) $(INCLUDES)

$(OBJS): | $(OBJDIR)

$(OBJDIR)%.o : %.c $(HEADER)
	$(CC) $(CFLAGS) -c $< -o $@ $(INCLUDES)

$(OBJDIR):
	@if [ ! -d "$(OBJDIR)" ]; then \
		mkdir $(OBJDIR);\
		fi

clean:
	rm -rf $(OBJS)
	rm -rf $(OBJDIR)

fclean: clean
	rm -rf $(NAME)

proper: all clean

re: fclean all
