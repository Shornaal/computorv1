/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edelangh <edelangh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 13:54:28 by edelangh          #+#    #+#             */
/*   Updated: 2015/05/10 13:55:37 by edelangh         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cpu.h"

void	ft_strcpy(char *a, char *b)
{
	int		i;

	i = -1;
	while (b[++i])
	{
		a[i] = b[i];
	}
	a[i] = b[i];
}

void	ft_nett_input(t_equa *t)
{
	int		i;

	i = -1;
	while (t->equa[++i])
	{
		if (t->equa[i] == ' ')
		{
			ft_strcpy(t->equa + i, t->equa + i + 1);
			--i;
		}
	}
}

void	ft_puts_input(t_equa *t)
{
	dprintf(1, "Reduced form:"
			" %f * X^0 + %f * X^1 + %f * X^2 = 0\n", t->c, t->b, t->a);
}

int		ft_next_digit(char *s, int i)
{
	while (s[i] == '-')
		++i;
	while (isdigit(s[i]))
		++i;
	if (s[i] == '.')
		++i;
	while (isdigit(s[i]))
		++i;
	return (i);
}

double	ft_atof(char *s, int i)
{
	double	r;
	int		sign;
	int		len;

	len = 1;
	r = 0;
	sign = 1;
	--i;
	while (s[++i] == '-')
		sign = -sign;
	--i;
	while (isdigit(s[++i]))
		r = (r * 10) + (s[i] - '0');
	if (s[i] == '.')
		while (isdigit(s[++i]))
		{
			r = (r * 10) + (s[i] - '0');
			len *= 10;
		}
	return (r * sign / len);
}
